//
//  RSSInteractor.swift
//  IOS RSS Parser
//
//  Created by Adi Wibowo on 07/04/20.
//  Copyright © 2020 Adi Wibowo. All rights reserved.
//

import Foundation
import RxSwift

class RSSInteractor: NSObject {
    var parserManager: XmlParserManager!
    var listRSS = [RSSItem]()
    
    convenience init(url: URL) {
        self.init(parserManager: XmlParserManager())
    }
    
    init(parserManager: XmlParserManager) {
        self.parserManager = parserManager
    }
    
    
    func getListRSS(url: URL) -> Observable<Void>{
        listRSS.removeAll()
        
        parserManager = XmlParserManager().initWithURL(url) as? XmlParserManager
        
        self.listRSS.append(contentsOf: parserManager.feedsArray)
        
        return Observable.of(())
        
    }
    
    
}
