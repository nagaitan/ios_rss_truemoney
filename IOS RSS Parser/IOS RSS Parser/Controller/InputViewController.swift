//
//  InputViewController.swift
//  IOS RSS Parser
//
//  Created by Adi Wibowo on 07/04/20.
//  Copyright © 2020 Adi Wibowo. All rights reserved.
//

import UIKit

class InputViewController: UIViewController {
  
    @IBOutlet weak var txtFiledURL: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Input RSS URL"
        txtFiledURL.addTarget(self, action: #selector(self.checkEmptyFields(_:)), for: .editingChanged)
        btnSubmit.cardify()
        btnSubmit.backgroundColor = UIColor.gray
    }
    
    @objc func checkEmptyFields(_ textField: UITextField) {
        if (txtFiledURL.text != "" && verifyUrl(urlString:txtFiledURL.text)) {
            btnSubmit.isEnabled = true
            btnSubmit.backgroundColor = UIColor.blue
        }
        else {
            btnSubmit.isEnabled = false
            btnSubmit.backgroundColor = UIColor.gray
        }
    }
    
    @IBAction func goToListRSS(_ sender: Any) {
            //redirect to List RSS
            let controller = ListRSSViewController.instantiate(url: self.txtFiledURL.text ?? "")
            controller.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url = NSURL(string: urlString.replacingOccurrences(of: " ", with: "")) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }

}
