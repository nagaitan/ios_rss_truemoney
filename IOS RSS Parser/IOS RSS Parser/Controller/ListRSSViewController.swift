//
//  ListRSSViewController.swift
//  IOS RSS Parser
//
//  Created by Adi Wibowo on 07/04/20.
//  Copyright © 2020 Adi Wibowo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SVPullToRefresh

class ListRSSViewController: UIViewController {
    var url : String?
    private let refreshStream = PublishSubject<Void>()
    private let disposeBag = DisposeBag()
    let interactor = RSSInteractor(parserManager: XmlParserManager())
    
    static func instantiate(url : String) -> ListRSSViewController {
        let controller = ListRSSViewController()
        controller.url = url.replacingOccurrences(of: " ", with: "")
        controller.navigationItem.title = "List RSS"
        return controller
    }

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupStream()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        refreshStream.onNext(())
    }
    
    func setupView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "ItemCell", bundle: nil), forCellReuseIdentifier: "ItemCell")
    }
    
    func setupStream() {
        tableView.addPullToRefresh {
            self.tableView.pullToRefreshView.startAnimating()
            self.refreshStream.onNext(())
        }
        
        refreshStream
            .do(onNext : { _ in
                
                self.loadingIndicator.isHidden = false
                self.loadingIndicator.startAnimating()
                self.interactor.listRSS.removeAll()
                self.tableView.reloadData()
            })
            .filter{
                return true
            }
            .flatMap { url in
                
                self.interactor.getListRSS(url: URL(string: self.url!)!)
            }
            .do( onNext : { _ in
                if self.interactor.listRSS.count == 0 {
                    self.showToast(message: "No Item Found")
                }
                self.loadingIndicator.isHidden = true
                self.loadingIndicator.stopAnimating()
                self.tableView.pullToRefreshView.stopAnimating()
                self.tableView.reloadData()
            }, onError : {
                errorType in
                print("Error Masuk")
                self.loadingIndicator.isHidden = true
                self.loadingIndicator.stopAnimating()
                self.tableView.pullToRefreshView.stopAnimating()

                self.tableView.reloadData()
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    

}

extension ListRSSViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.interactor.listRSS.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
        
        cell.bindItem(item: self.interactor.listRSS[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 73
    }
    
    
}
