//
//  RSSItem.swift
//  IOS RSS Parser
//
//  Created by Adi Wibowo on 07/04/20.
//  Copyright © 2020 Adi Wibowo. All rights reserved.
//

import Foundation

struct RSSItem {
    var title : String?
    var subTitle : String?
    var image : String?
    var link : String?
}
