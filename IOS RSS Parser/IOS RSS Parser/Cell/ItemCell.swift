//
//  ItemCell.swift
//  IOS RSS Parser
//
//  Created by Adi Wibowo on 07/04/20.
//  Copyright © 2020 Adi Wibowo. All rights reserved.
//

import UIKit
import AlamofireImage

class ItemCell: UITableViewCell {
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewShadow: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        viewShadow.cardify()
        imgItem.cardify()
        // Configure the view for the selected state
    }
    
    func bindItem(item : RSSItem) {
        lblTitle.text = item.title ?? ""
        if let sub = item.subTitle, sub.contains("img src") {
            let descArr = sub.components(separatedBy: ">")
            if descArr.count >= 1 {
                lblSubTitle.text = descArr[1]
            }
        }else{
            lblSubTitle.text = item.subTitle ?? ""
        }
    
        if let imgUrl = item.image, let url = URL(string: imgUrl) {
            imgItem.af.setImage(withURL: url, placeholderImage: UIImage(named: "placeholder"), filter: nil, progress: nil, imageTransition: .crossDissolve(0.3), runImageTransitionIfCached: true, completion: nil)
        }
    }
    
    
    
}
