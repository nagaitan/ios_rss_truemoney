//
//  XmlParserManager.swift
//  Rsswift
//
//  Created by Arled Kola on 18/11/2016, Edited By Adi Wibowo on 07/03/2020
//  Copyright © 2016 ArledKola. All rights reserved.
//
import Foundation

class XmlParserManager: NSObject, XMLParserDelegate {
    
    var parser = XMLParser()
    var feeds = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var ftitle = NSMutableString()
    var link = NSMutableString()
    var fdescription = NSMutableString()
    var fdate = NSMutableString()
    var fimagesFromDescription : [String]? = nil
    var imageFromThumbnail = ""
    var imageFromContent = ""
    var feedsArray = [RSSItem]()
    
    // initilise parser
    func initWithURL(_ url :URL) -> AnyObject {
        startParse(url)
        return self
    }
    
    func startParse(_ url :URL) {
        feeds = []
        parser = XMLParser(contentsOf: url)!
        parser.delegate = self
        parser.shouldProcessNamespaces = false
        parser.shouldReportNamespacePrefixes = false
        parser.shouldResolveExternalEntities = false
        parser.parse()
    }
    
    func allFeeds() -> NSMutableArray {
        return feeds
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        element = elementName as NSString
        if (element as NSString).isEqual(to: "item") {
            elements =  NSMutableDictionary()
            elements = [:]
            ftitle = NSMutableString()
            ftitle = ""
            link = NSMutableString()
            link = ""
            fdescription = NSMutableString()
            fdescription = ""
            fdate = NSMutableString()
            fdate = ""
        } else if (element as NSString).isEqual(to: "enclosure") {
            if let urlString = attributeDict["url"] {
                imageFromThumbnail = urlString
            }
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "item") {
            var item = RSSItem()
            
            if ftitle != "" {
                elements.setObject(ftitle, forKey: "title" as NSCopying)
                item.title = ftitle as String
            }
            if link != "" {
                elements.setObject(link, forKey: "link" as NSCopying)
                item.link = link as String
            }
            if fdescription != "" {
                elements.setObject(fdescription, forKey: "description" as NSCopying)
                item.subTitle = fdescription as String
            }
            if fdate != "" {
                elements.setObject(fdate, forKey: "pubDate" as NSCopying)
            }
            if fimagesFromDescription?.count != 0 {
                if let imgs = fimagesFromDescription, imgs.count > 0 {
                elements.setObject(imgs[0], forKey: "imagesFromDescription" as NSCopying)
                item.image = imgs[0]
                }
            }
            if imageFromThumbnail != "" {
                elements.setObject(imageFromThumbnail, forKey: "imageFromThumbnail" as NSCopying)
                item.image = imageFromThumbnail            }
            
            feedsArray.append(item)
            feeds.add(elements)
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if element.isEqual(to: "title") {
            ftitle.append(string)
        } else if element.isEqual(to: "link") {
            link.append(string)
        } else if element.isEqual(to: "description") {
            fdescription.append(string)
            fimagesFromDescription = self.imagesFromHTMLString(fdescription as String)
        } else if element.isEqual(to: "pubDate") {
            fdate.append(string)
        }
    }
    
    fileprivate func imagesFromHTMLString(_ htmlString: String) -> [String] {
        let htmlNSString = htmlString as NSString;
        var images: [String] = Array();
        
        do {
            let regex = try NSRegularExpression(pattern: "(https?)\\S*(png|jpg|jpeg|gif)", options: [NSRegularExpression.Options.caseInsensitive])
        
            regex.enumerateMatches(in: htmlString, options: [NSRegularExpression.MatchingOptions.reportProgress], range: NSMakeRange(0, htmlString.count)) { (result, flags, stop) -> Void in
                if let range = result?.range {
                    images.append(htmlNSString.substring(with: range))  //because Swift ranges are still completely ridiculous
                }
            }
        }
        
        catch {
            
        }
        
        return images;
    }
}
