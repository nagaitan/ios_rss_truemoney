//
//  IOS_RSS_ParserUITests.swift
//  IOS RSS ParserUITests
//
//  Created by Adi Wibowo on 07/04/20.
//  Copyright © 2020 Adi Wibowo. All rights reserved.
//

import XCTest

class IOS_RSS_ParserUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        app = XCUIApplication()
        app.launch()
        
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
        app = nil
    }

    func testSubmitButtonDisabledAfterLaunch() throws {
        let isEnabled = app.buttons["Submit"].isEnabled
        XCTAssertFalse(isEnabled)
    }
    
    func testSubmitButtonEnablesAfterFillingInvalidURL() {
        app.textFields["Input RSS Feed URL Here"].tap()
        app.textFields["Input RSS Feed URL Here"].typeText("htt")
        let isEnabledAfterFillingURL = app.buttons["Submit"].isEnabled
        XCTAssertFalse(isEnabledAfterFillingURL)
    }
    
    func testSubmitButtonEnablesAfterFillingValidURL() {
        app.textFields["Input RSS Feed URL Here"].tap()
        app.textFields["Input RSS Feed URL Here"].typeText("https://google.com")
        let isEnabledAfterFillingURL = app.buttons["Submit"].isEnabled
        XCTAssertTrue(isEnabledAfterFillingURL)
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
