//
//  IOS_RSS_ParserTests.swift
//  IOS RSS ParserTests
//
//  Created by Adi Wibowo on 07/04/20.
//  Copyright © 2020 Adi Wibowo. All rights reserved.
//

import XCTest
@testable import IOS_RSS_Parser

class IOS_RSS_ParserTests: XCTestCase {
    var sut: InputViewController!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = InputViewController()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func testCheckURLShouldReturnFalse(){
        let invalidURL = "salah"
        let result1 = sut.verifyUrl(urlString: invalidURL)
        XCTAssertFalse(result1, "Positive result for invalid url")
    }
    
    func testCheckURLShouldReturnTrue(){
        let validURL = "https://www.antaranews.com/rss/hiburan"
        let result1 = sut.verifyUrl(urlString: validURL)
        XCTAssertTrue(result1, "Positive result for valid url")
    }
}
