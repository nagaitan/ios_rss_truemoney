# README #

### About : ###

* This app parses xml of RSS and shows RSS Item as List. Each Item consists of Image, Title and Subtitle.  You can see some screenshots of the app above in Screenshot Folder.

### APP RSS URL Parser, fetaures : ###

* Input Url, automatically Enable & Disable Button based on URL Validity
* Load RSS List, Parse & Shown as List 
* List completed with Image 
* Table View Refresh to Load 
* Framework (Design Pattern) : VIPER with Reactive Swift
* Unit Test
* UI Test
* Clean Code, no Error no Warning

### 3rd Party ###

* Alamofire
* Alamofire Image
* RxSwift
* RxCocoa
* SVPullToRefresh

### Creator ###

* Adi Wibowo P.
* Email me : adiwibowoplus@gmail.com
